Quick guide on how to use this overlay.

1. Install the fonts (casting_overlay_overwatch/fonts).
2. Launch OBS, click 'Scene Collection' and import 'casting_overlay_overwatch.json'. (casting_overlay_overwatch/casting_overlay_overwatch.json)
3. Click 'Scene Collection' > 'casting_overlay_overwatch'
4. Select 'match_overlay' scene, update path for source 'match_main_overlay'.  (casting_overlay_overwatch/images/match_main_overlay.png)
5. Select 'match_teams_normal' scene, update path for source 'team_one_name' (casting_overlay_overwatch/team_one_name.txt) etc. **
6. Optionally update path for relevant background images in 'live_action' and 'live_starting_soon' scenes.

** note, there is no need to update the 'match_teams_inverse' scene, as it is linked to 'match_teams_normal'

Default hotkey to switch teams Ctrl + Alt + Home and Ctrl + Alt + End